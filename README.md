# How to compile the bridge

#### To build on your current platform (macOS, Windows)

> [!IMPORTANT]
> You should have install or build PCL dependencies on your computer

```bash
mkdir build
cd build
cmake ..
cmake --build
```

#### To build on iOS

> [!IMPORTANT]
> You need to setup a "thirdparty" folder with the includes and the libs
> You can use this [repo](https://gitlab.com/flatpixel/pcl-superbuild.git) to build PCL for iOS

```bash
mkdir thirdparty
mkdir build
cd build
cmake .. -DCMAKE_SYSTEM_NAME=iOS \
-DCMAKE_OSX_ARCHITECTURES=arm64 \
-DCMAKE_OSX_DEPLOYMENT_TARGET=11.0 \
-DBUILD_SHARED_LIBS=OFF \
-DCMAKE_BUILD_TYPE=Release
cmake --build
```
