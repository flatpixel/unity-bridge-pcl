#pragma once

#ifdef _WIN32
#ifdef WIN_EXPORT
#define TESTDLL_API __declspec(dllexport)
#else
#define TESTDLL_API __declspec(dllimport)
#endif
#else
#define TESTDLL_API
#endif

extern "C"
{
	struct Vector3
	{
		float x;
		float y;
		float z;
	};

	TESTDLL_API void Toto();
	TESTDLL_API void Titi();
	TESTDLL_API void AllocateAndComputeMesh(Vector3 *points, int32_t size, int32_t **resultTrianglesPtr, int32_t *sizeResult,
                            double searchRadius = 0.025, double multiplier = 2.5, int32_t maximumNearestNeighbors = 300, double maximumSurfaceAngle = M_PI / 4,
                            double minAngle = M_PI / 18, double maxAngle = 2 * M_PI / 3);
	TESTDLL_API void ReleaseComputedMesh(int32_t *resultTrianglesPtr);
}