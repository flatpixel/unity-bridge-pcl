
#include <iostream>
#include <vector>
#include <ctime>

#include "../include/pclInterface.h"
#undef min
#undef max
#include "pcl/point_cloud.h"
#include "pcl/point_types.h"
#include "pcl/octree/octree_search.h"
#include "pcl/io/pcd_io.h"
#include "pcl/kdtree/kdtree_flann.h"
#include "pcl/features/normal_3d.h"
#include "pcl/surface/gp3.h"

void Toto()
{
    srand((unsigned int)time(NULL));
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

    // Generate pointcloud data
    cloud->width = 100;
    cloud->height = 1;
    cloud->points.resize(cloud->width * cloud->height);

    for (std::size_t i = 0; i < cloud->size(); ++i)
    {
        (*cloud)[i].x = 1024.0f * rand() / (RAND_MAX + 1.0f);
        (*cloud)[i].y = 1024.0f * rand() / (RAND_MAX + 1.0f);
        (*cloud)[i].z = 1024.0f * rand() / (RAND_MAX + 1.0f);
    }

    std::cerr << "-4" << std::endl;

    //// Load input file into a PointCloud<T> with an appropriate type
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    //pcl::PCLPointCloud2 cloud_blob;
    //pcl::io::loadPCDFile("bun0.pcd", cloud_blob);
    //pcl::fromPCLPointCloud2(cloud_blob, *cloud);
    //* the data should be available in cloud

    std::cerr << "-3" << std::endl;

    // Normal estimation*
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> n;
    pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud(cloud);
    n.setInputCloud(cloud);
    n.setSearchMethod(tree);
    n.setKSearch(20);
    n.compute(*normals);
    //* normals should not contain the point normals + surface curvatures

    std::cerr << "-2" << std::endl;

    // Concatenate the XYZ and normal fields*
    pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals(new pcl::PointCloud<pcl::PointNormal>);
    pcl::concatenateFields(*cloud, *normals, *cloud_with_normals);
    //* cloud_with_normals = cloud + normals

    std::cerr << "-1" << std::endl;

    // Create search tree*
    pcl::search::KdTree<pcl::PointNormal>::Ptr tree2(new pcl::search::KdTree<pcl::PointNormal>);
    tree2->setInputCloud(cloud_with_normals);

    std::cerr << "0" << std::endl;

    // Initialize objects
    pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
    pcl::PolygonMesh triangles;

    std::cerr << "1" << std::endl;

    // Set the maximum distance between connected points (maximum edge length)
    gp3.setSearchRadius(0.025);

    std::cerr << "2" << std::endl;

    // Set typical values for the parameters
    gp3.setMu(2.5);
    gp3.setMaximumNearestNeighbors(100);
    gp3.setMaximumSurfaceAngle(M_PI / 4); // 45 degrees
    gp3.setMinimumAngle(M_PI / 18);       // 10 degrees
    gp3.setMaximumAngle(2 * M_PI / 3);    // 120 degrees
    gp3.setNormalConsistency(false);

    std::cerr << "3" << std::endl;

    // Get result
    gp3.setInputCloud(cloud_with_normals);
    gp3.setSearchMethod(tree2);
    gp3.reconstruct(triangles);

    std::cerr << "4" << std::endl;

    // Additional vertex information
    std::vector<int> parts = gp3.getPartIDs();
    std::vector<int> states = gp3.getPointStates();

    std::cerr << "5" << std::endl;

    return;
}

void Titi()
{
    srand((unsigned int)time(NULL));

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

    // Generate pointcloud data
    cloud->width = 1000;
    cloud->height = 1;
    cloud->points.resize(cloud->width * cloud->height);

    for (std::size_t i = 0; i < cloud->size(); ++i)
    {
        (*cloud)[i].x = 1024.0f * rand() / (RAND_MAX + 1.0f);
        (*cloud)[i].y = 1024.0f * rand() / (RAND_MAX + 1.0f);
        (*cloud)[i].z = 1024.0f * rand() / (RAND_MAX + 1.0f);
    }

    float resolution = 128.0f;

    pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(resolution);

    octree.setInputCloud(cloud);
    octree.addPointsFromInputCloud();

    pcl::PointXYZ searchPoint;

    searchPoint.x = 1024.0f * rand() / (RAND_MAX + 1.0f);
    searchPoint.y = 1024.0f * rand() / (RAND_MAX + 1.0f);
    searchPoint.z = 1024.0f * rand() / (RAND_MAX + 1.0f);

    // Neighbors within voxel search

    std::vector<int> pointIdxVec;

    if (octree.voxelSearch(searchPoint, pointIdxVec))
    {
        std::cout << "Neighbors within voxel search at (" << searchPoint.x
                  << " " << searchPoint.y
                  << " " << searchPoint.z << ")"
                  << std::endl;

        for (std::size_t i = 0; i < pointIdxVec.size(); ++i)
            std::cout << "    " << (*cloud)[pointIdxVec[i]].x
                      << " " << (*cloud)[pointIdxVec[i]].y
                      << " " << (*cloud)[pointIdxVec[i]].z << std::endl;
    }

    // K nearest neighbor search

    int K = 10;

    std::vector<int> pointIdxNKNSearch;
    std::vector<float> pointNKNSquaredDistance;

    std::cout << "K nearest neighbor search at (" << searchPoint.x
              << " " << searchPoint.y
              << " " << searchPoint.z
              << ") with K=" << K << std::endl;

    if (octree.nearestKSearch(searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0)
    {
        for (std::size_t i = 0; i < pointIdxNKNSearch.size(); ++i)
            std::cout << "    " << (*cloud)[pointIdxNKNSearch[i]].x
                      << " " << (*cloud)[pointIdxNKNSearch[i]].y
                      << " " << (*cloud)[pointIdxNKNSearch[i]].z
                      << " (squared distance: " << pointNKNSquaredDistance[i] << ")" << std::endl;
    }

    // Neighbors within radius search

    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;

    float radius = 256.0f * rand() / (RAND_MAX + 1.0f);

    std::cout << "Neighbors within radius search at (" << searchPoint.x
              << " " << searchPoint.y
              << " " << searchPoint.z
              << ") with radius=" << radius << std::endl;

    if (octree.radiusSearch(searchPoint, radius, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0)
    {
        for (std::size_t i = 0; i < pointIdxRadiusSearch.size(); ++i)
            std::cout << "    " << (*cloud)[pointIdxRadiusSearch[i]].x
                      << " " << (*cloud)[pointIdxRadiusSearch[i]].y
                      << " " << (*cloud)[pointIdxRadiusSearch[i]].z
                      << " (squared distance: " << pointRadiusSquaredDistance[i] << ")" << std::endl;
    }
}

void ReleaseComputedMesh(int *resultTrianglesPtr)
{
    delete [] resultTrianglesPtr;
}

void AllocateAndComputeMesh(Vector3 *points, int32_t size, int32_t **resultTrianglesPtr, int32_t *sizeResult,
                            double searchRadius, double multiplier, int32_t maximumNearestNeighbors, double maximumSurfaceAngle,
                            double minAngle, double maxAngle)
{
    // std::cerr << "size = " << size << std::endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    cloud->points.resize(size);

    // std::cerr << "pointcloud size = " << cloud->size() << std::endl;

    for (std::size_t i = 0; i < cloud->size(); ++i)
    {
        (*cloud)[i].x = points[i].x;
        (*cloud)[i].y = points[i].y;
        (*cloud)[i].z = points[i].z;
    }

    // std::cerr << "pointcloud initialized size = " << cloud->size() << std::endl;

    // Normal estimation*
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> n;
    pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);

    // std::cerr << "tree->setInputCloud" << std::endl;
    tree->setInputCloud(cloud);

    // std::cerr << "n.setInputCloud" << std::endl;
    n.setInputCloud(cloud);
    n.setSearchMethod(tree);
    n.setKSearch(20);
    n.compute(*normals);
    //* normals should not contain the point normals + surface curvatures

    // std::cerr << "pointcloud normals size = " << normals->size() << std::endl;

    // Concatenate the XYZ and normal fields*
    pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals(new pcl::PointCloud<pcl::PointNormal>);
    pcl::concatenateFields(*cloud, *normals, *cloud_with_normals);
    //* cloud_with_normals = cloud + normals
    // std::cerr << "Concatenate the XYZ and normal fields size = " << cloud_with_normals->size() << std::endl;

    // Create search tree*
    pcl::search::KdTree<pcl::PointNormal>::Ptr tree2(new pcl::search::KdTree<pcl::PointNormal>);
    tree2->setInputCloud(cloud_with_normals);
    // std::cerr << "tree2->setInputCloud" << std::endl;

    // Initialize objects
    pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
    pcl::PolygonMesh triangles;
    // std::cerr << "gp3 & triangles init" << std::endl;

    // Set the maximum distance between connected points (maximum edge length)
    gp3.setSearchRadius(searchRadius);

    // Set typical values for the parameters
    gp3.setMu(multiplier);
    gp3.setMaximumNearestNeighbors(maximumNearestNeighbors);
    gp3.setMaximumSurfaceAngle(maximumSurfaceAngle); // 45 degrees
    gp3.setMinimumAngle(minAngle);       // 10 degrees
    gp3.setMaximumAngle(maxAngle);    // 120 degrees
    gp3.setNormalConsistency(false);

    // std::cerr << "set gp3 parameters for search" << std::endl;
    // std::cerr << "set gp3 searchRadius " << searchRadius << std::endl;
    // std::cerr << "set gp3 multiplier " << multiplier << std::endl;
    // std::cerr << "set gp3 maximumNearestNeighbors " << maximumNearestNeighbors << std::endl;
    // std::cerr << "set gp3 maximumSurfaceAngle " << maximumSurfaceAngle << std::endl;
    // std::cerr << "set gp3 minAngle " << minAngle << std::endl;
    // std::cerr << "set gp3 maxAngle " << maxAngle << std::endl;

    // Get result
    gp3.setInputCloud(cloud_with_normals);
    // std::cerr << "gp3.setInputCloud" << std::endl;

    gp3.setSearchMethod(tree2);
    // std::cerr << "gp3.setSearchMethod" << std::endl;

    gp3.reconstruct(triangles);
    // std::cerr << "gp3.reconstruct" << std::endl;

    std::vector<::pcl::Vertices> poly = triangles.polygons;
    int nbPoly = triangles.polygons.size();
    // std::cerr << "get polygon data from std vector " << nbPoly << std::endl;

    // std::Vector<int32_t> resultTriangles;
    // int32_t resultTriangles[nbPoly * 3];
    int32_t *resultTriangles = new int32_t[nbPoly * 3];
    int32_t index = 0;
    for (size_t i = 0; i < nbPoly; i++)
    {
        int verticeSize = poly[i].vertices.size();
        if (verticeSize > 3)
            std::cerr << "Error, verticeSize should not be > 3: " << verticeSize << std::endl;

        for (size_t j = 0; j < verticeSize; j++)
        {
            // resultTriangles.push_back((int32_t)(poly[i].vertices[j]));
            resultTriangles[index++] = (int32_t)(poly[i].vertices[j]);
            // if (i <= 100
            // || (index > 3500 && index < 3600)
            // || poly[i].vertices[j] > 1000)
            // {
            //     std::cerr << "poly[i].vertices[j] " << poly[i].vertices[j] << " at index : " << index - 1 << std::endl;
            //     // std::cerr << "resultTriangles copy " << resultTriangles[index-1] << std::endl;
            // }
        }
    }

    if (index != nbPoly * 3)
        std::cerr << "Error, index should not be != nbPoly * 3 (" << index << "!=" << nbPoly * 3 << ")" << std::endl;

    // std::cerr << "nb indices " << nbPoly * 3 << " index : " << index << std::endl;

    *sizeResult = nbPoly * 3;
    *resultTrianglesPtr = resultTriangles;

    // *resultTrianglesPtr = resultTriangles;
    // *sizeResult = (int32_t)resultTriangles.size();
    // *resultTrianglesPtr = resultTriangles.data();

    //// Additional vertex information
    //std::vector<int> parts = gp3.getPartIDs();
    //std::vector<int> states = gp3.getPointStates();
}
